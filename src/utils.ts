export const zeroOrPositive = (value: number) => (value < 0 ? 0 : value);

export const randomChoice = <T>(choices: T[]): T => {
  const index = Math.floor(Math.random() * choices.length);
  return choices[index];
};

export const defaultValueIfBelow = (
  value: number,
  bound: number,
  defaultValue: number
) => (value < bound ? defaultValue : value);
