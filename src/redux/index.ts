import {
  Action,
  applyMiddleware,
  combineReducers,
  createStore,
  Dispatch,
} from 'redux';
import common, { CommonState } from './common/reducer';
import { AppInit, PaddleClick, ToggleAutoPaddle } from './common';
import { combineEpics, createEpicMiddleware } from 'redux-observable';
import { commonEpic$ } from './common/epics';
import event, { EventState } from './event/reducer';
import { getCurrentRole } from './common/selectors';
import { eventsEpic$ } from './event/epics';
import { DenyEvent, SubmitEvent } from './event/actions';

const classInstanceActionsMiddleware = () => next => action => {
  next(action && typeof action === 'object' ? { ...action } : action);
};

const epicMiddleware = createEpicMiddleware<Action, Action, AppState, any>();
const rootEpic = combineEpics(commonEpic$, eventsEpic$);
export const configureStore = () => {
  const store = createStore(
    combineReducers({ common, event }),
    applyMiddleware(classInstanceActionsMiddleware, epicMiddleware)
  );
  epicMiddleware.run(rootEpic);
  return store;
};

export interface AppState {
  common: CommonState;
  event: EventState;
}

export const mapStateToProps = (state: AppState) => ({
  ...state,
  role: getCurrentRole(state),
});
export const mapDispatchToProps = (dispatch: Dispatch) => ({
  paddleClick: () => dispatch(new PaddleClick()),
  togglePassiveIncome: (active: boolean) =>
    dispatch(new ToggleAutoPaddle({ active })),
  onAppInit: () => dispatch(new AppInit()),
  onSubmitEvent: () => dispatch(new SubmitEvent()),
  onDenyEvent: () => dispatch(new DenyEvent()),
});
