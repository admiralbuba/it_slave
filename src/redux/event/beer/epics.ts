import { ActionsObservable, combineEpics, ofType } from 'redux-observable';
import {
  EndEvent,
  EventActions,
  EventNames,
  OpenEventBox,
  StartEvent,
} from '../actions';
import { EMPTY, Observable, timer } from 'rxjs';
import { ofEventType } from '../../redux-helper';
import { map, mapTo, switchMap } from 'rxjs/operators';
import {
  BeerActions,
  BeerActionTypes,
  BeerEnded,
  BeerStarted,
} from './actions';
import { PaddleEvent, Priority } from '../../../types';
import { waitForAnswer } from '../operators';
import { AddPassiveIncome, CommonActions } from '../../common';

export const beerEvent = new PaddleEvent(EventNames.BEER, Priority.MEDIUM, {
  title: 'По пивку',
  denyText: 'Лучше на трезвую голову поработать',
  description: 'Коллеги предложили “по певку”',
  submitText: 'Согласиться, под литры лучше гребётся',
});

const SUDDEN_MANAGER_TIMER = 10000;
const SUDDEN_MANAGER_PROBABILITY = 0.5;

const onBeerEvent$ = (
  action$: ActionsObservable<EventActions>
): Observable<BeerActions> =>
  action$.pipe(
    ofEventType(EventNames.BEER),
    mapTo(new BeerStarted())
  );

const beerEventStarted$ = (
  action$: ActionsObservable<BeerActions>
): Observable<EventActions> =>
  action$.pipe(
    ofType(BeerActionTypes.BEER_STARTED),
    mapTo(new OpenEventBox(beerEvent.details))
  );

const beerEvent$ = (
  action$: ActionsObservable<BeerActions>
): Observable<CommonActions | EventActions | BeerActions> =>
  action$.pipe(
    ofType(BeerActionTypes.BEER_STARTED),
    switchMap(() => waitForAnswer(action$)),
    switchMap(result =>
      !result
        ? EMPTY
        : [new AddPassiveIncome({ value: 1, time: 100000 }), new BeerEnded()]
    )
  );

const beerEventEnded$ = (
  action$: ActionsObservable<BeerActions>
): Observable<EventActions> =>
  action$.pipe(
    ofType(BeerActionTypes.BEER_ENDED),
    mapTo(new EndEvent())
  );

const suddenManagerTimer$ = (
  action$: ActionsObservable<BeerActions>
): Observable<any> =>
  action$.pipe(
    ofType(BeerActionTypes.BEER_ENDED),
    switchMap(() =>
      timer(SUDDEN_MANAGER_TIMER).pipe(
        map(() => Math.random() <= SUDDEN_MANAGER_PROBABILITY)
      )
    ),
    switchMap(prob =>
      prob
        //  @Fixme
        ? [new StartEvent({ name: EventNames.SUDDEN_MANAGER }), new EndEvent()]
        : EMPTY
    )
  );

export const beerEpic$ = combineEpics(
  onBeerEvent$,
  beerEventStarted$,
  beerEvent$,
  beerEventEnded$,
  suddenManagerTimer$
);
