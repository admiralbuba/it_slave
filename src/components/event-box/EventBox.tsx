import React from 'react';
import { Button } from 'antd';
import paddle from './double-paddle.png';
import './event-box.scss';
import { EventDetails } from '../../types';

export interface EventBoxProps {
  opened: boolean;
  event?: EventDetails;
  onSubmit: () => void;
  onDeny: () => void;
}
export const EventBox = ({
  event,
  opened,
  onSubmit,
  onDeny,
}: EventBoxProps) => {
  if (!opened || !event) {
    return null;
  }
  return (
    <div className="event-box-component">
      <img className="double-paddle-icon" alt="double paddle" src={paddle} />
      <div className="event-box-container">
        <div className="event-box-content">
          <h2>{event.title}</h2>
          <p className="event-box-description">{event.description}</p>
          <div className="event-box-action-buttons">
            {event.denyText && (
              <div className="action-button-container">
                <Button
                  type="danger"
                  className="attention-btn"
                  onClick={onDeny}
                >
                  <span className="event-box-action-button-text">
                    {event.denyText}
                  </span>
                </Button>
              </div>
            )}
            {event.submitText && (
              <div className="action-button-container">
                <Button
                  type="primary"
                  className="action-btn"
                  onClick={onSubmit}
                >
                  <span className="event-box-action-button-text">
                    {event.submitText}
                  </span>
                </Button>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default EventBox;
