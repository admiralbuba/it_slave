import { CommonActions, CommonActionTypes } from './actions';
import { Progress, Role } from '../../components/progress-list/ProgressList';
import { defaultValueIfBelow } from '../../utils';

export interface CommonState {
  value: number;
  cps: number;
  roles: Role[];
  started: boolean;
  paddleDisabled: boolean;
  autoPaddleEnabled: boolean;
}

const roles: Role[] = [
  {
    title: 'Slave Software Grebec ',
    amount: 0,
    status: Progress.IN_PROGRESS,
    id: '1',
  },
  {
    title: 'Junior Software Grebec Engineer',
    amount: 30,
    status: Progress.IN_PROGRESS,
    id: '2',
  },
  {
    title: 'Senior Software Grebec Engineer',
    amount: 100,
    status: Progress.IN_PROGRESS,
    id: '3',
  },
  {
    title: 'Chief Software Grebec Engineer',
    amount: 200,
    status: Progress.IN_PROGRESS,
    id: '4',
  },
  {
    title: 'Slave Solution Architect',
    amount: 300,
    status: Progress.IN_PROGRESS,
    id: '5',
  },
  {
    title: 'Slave Department Director',
    amount: 400,
    status: Progress.IN_PROGRESS,
    id: '6',
  },
  {
    title: 'Head of Global Slavery',
    amount: 500,
    status: Progress.IN_PROGRESS,
    id: '7',
  },
  {
    title: '(CEO)Chief Executive Officer & President of Slavery',
    amount: 600,
    status: Progress.IN_PROGRESS,
    id: '8',
  },
];

export const initialState = {
  value: 0,
  cps: 1,
  roles: roles,
  started: false,
  paddleDisabled: false,
  autoPaddleEnabled: false,
};

export function commonReducer(
  state: CommonState = initialState,
  action: CommonActions
) {
  switch (action.type) {
    case CommonActionTypes.ADD_INCOME:
      const { value } = action.payload;
      return {
        ...state,
        value: defaultValueIfBelow(value + state.value, 0, 0),
      };
    case CommonActionTypes.SET_PROGRESS:
      return { ...state, roles: action.payload };
    case CommonActionTypes.GAME_START:
      return { ...state, started: true };
    case CommonActionTypes.GAME_END:
      return { ...state, started: false };
    case CommonActionTypes.RESET_INCOME:
      return { ...state, value: 0 };
    case CommonActionTypes.ADD_PASSIVE_INCOME:
      return {
        ...state,
        cps: defaultValueIfBelow(state.cps + action.payload.value, 1, 1),
      };
    case CommonActionTypes.DISABLE_PADDLE:
      return { ...state, paddleDisabled: true };
    case CommonActionTypes.ENABLE_PADDLE:
      return { ...state, paddleDisabled: false };
    case CommonActionTypes.AUTO_PADDLE_TOGGLE:
      return { ...state, autoPaddleEnabled: action.payload.active };
  }
  return state;
}

export default commonReducer;
