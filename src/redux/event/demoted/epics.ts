import { PaddleEvent, Priority } from '../../../types';
import {
  EndEvent,
  EventActions,
  EventNames,
  OpenEventBox,
  StartEvent,
} from '../actions';
import {
  ActionsObservable,
  combineEpics,
  ofType,
  StateObservable,
} from 'redux-observable';
import { Observable } from 'rxjs';
import { ofEventType } from '../../redux-helper';
import { map, mapTo, switchMap, withLatestFrom } from 'rxjs/operators';
import {
  DemotedActions,
  DemotedActionTypes,
  DemotedEnded,
  DemotedStarted,
} from './actions';
import { waitForAnswer } from '../operators';
import { AddIncome, CommonActions } from '../../common';
import { AppState } from '../../index';
import { getCurrentRole, getIncome, getRoles } from '../../common/selectors';
import { getRoleAtDirection } from '../../common/operators';

export const demotedEvent = new PaddleEvent(
  EventNames.DEMOTED,
  Priority.SPECIAL,
  {
    title: 'РАЗЖАЛОВАН',
    denyText: 'Вот суки! Надо бы успокоиться',
    description:
      'Вас понизили в вашей должности, погонщик вами недоволен, а погонщик всегда прав',
    submitText: 'Я всё наверстаю',
  }
);

const onDemotedEvent$ = (
  action$: ActionsObservable<EventActions>
): Observable<DemotedActions> =>
  action$.pipe(
    ofEventType(EventNames.DEMOTED),
    mapTo(new DemotedStarted())
  );

const demotedEventStarted$ = (
  action$: ActionsObservable<DemotedActions>
): Observable<EventActions> =>
  action$.pipe(
    ofType(DemotedActionTypes.DEMOTED_STARTED),
    mapTo(new OpenEventBox(demotedEvent.details))
  );

const demotedEvent$ = (
  action$: ActionsObservable<DemotedActions>,
  state$: StateObservable<AppState>
): Observable<EventActions | CommonActions | DemotedActions> =>
  action$.pipe(
    ofType(DemotedActionTypes.DEMOTED_STARTED),
    switchMap(() => waitForAnswer(action$)),
    map(result =>
      result
        ? new AddIncome({ value: 50 })
        : new StartEvent({ name: EventNames.SMOKE_BREAK })
    ),
    withLatestFrom(state$),
    switchMap(([action, state]) => {
      const currentRole = getCurrentRole(state);
      const roles = getRoles(state);
      const currentIncome = getIncome(state);
      const role = getRoleAtDirection(roles.slice(), currentRole, -1);
      return [
        action,
        new AddIncome({ value: role.amount - currentIncome }),
        new DemotedEnded(),
      ];
    })
  );

const demotedEventEnded$ = (
  action$: ActionsObservable<DemotedActions>
): Observable<EventActions | CommonActions | DemotedActions> =>
  action$.pipe(
    ofType(DemotedActionTypes.DEMOTED_ENDED),
    mapTo(new EndEvent())
  );
export const demotedEpic$ = combineEpics(
  onDemotedEvent$,
  demotedEventStarted$,
  demotedEvent$,
  demotedEventEnded$
);
