import { PaddleEvent, Priority } from '../../../types';
import {
  EndEvent,
  EventActions,
  EventNames,
  OpenEventBox,
  StartEvent,
} from '../actions';
import {
  ActionsObservable,
  combineEpics,
  ofType,
  StateObservable,
} from 'redux-observable';
import { Observable } from 'rxjs';
import { DemotedActions } from '../demoted/actions';
import { ofEventType } from '../../redux-helper';
import { map, mapTo, switchMap, withLatestFrom } from 'rxjs/operators';
import {
  FindTraitor,
  FoodStolenActions,
  FoodStolenActionTypes,
  FoodStolenEnded,
  FoodStolenStarted,
  StayHungry,
} from './actions';
import { AppState } from '../../index';
import { waitForAnswer } from '../operators';
import { AddIncome, AddPassiveIncome, CommonActions } from '../../common';
import { getEvent } from '../selectors';
import { getCPS } from '../../common/selectors';

export const foodStolenEvent = new PaddleEvent(
  EventNames.FOOD_STOLEN,
  Priority.MEDIUM,
  {
    title: 'У вас украли ссобойку',
    denyText: 'Вот ссука! Голодному грести теперь',
    description:
      'Айтишники, конечно, “элита нации” , но еду с кухни пи.. похищают на раз-два. К сожалению, на этот раз участь постигла и вашу ссобойку',
    submitText: 'Попытаться найти суку',
  }
);

export const FIND_TRAITOR_PROB = 0.5;

const onFoodStolenEvent$ = (
  action$: ActionsObservable<EventActions>
): Observable<FoodStolenActions> =>
  action$.pipe(
    ofEventType(EventNames.FOOD_STOLEN),
    mapTo(new FoodStolenStarted())
  );

const foodStolenEventStarted$ = (
  action$: ActionsObservable<FoodStolenActions>,
  state$: StateObservable<AppState>
): Observable<EventActions> =>
  action$.pipe(
    ofType(FoodStolenActionTypes.FOOD_STOLEN_STARTED),
    mapTo(new OpenEventBox(foodStolenEvent.details))
  );

const foodStolenEvent$ = (
  action$: ActionsObservable<FoodStolenActions>
): Observable<FoodStolenActions> =>
  action$.pipe(
    ofType(FoodStolenActionTypes.FOOD_STOLEN_STARTED),
    switchMap(() => waitForAnswer(action$)),
    map(result => (result ? new StayHungry() : new FindTraitor())),
    switchMap(action => [action, new FoodStolenEnded()])
  );

const findTraitor$ = (
  action$: ActionsObservable<FoodStolenActions>,
  state$: StateObservable<AppState>
): Observable<EventActions> =>
  action$.pipe(
    ofType(FoodStolenActionTypes.FIND_TRAITOR),
    withLatestFrom(state$),
    map(([, state]) => {
      const event = getEvent(state, EventNames.FRIENDLY_MANAGER);
      // Fixme
      const prob = Math.random();
      return prob <= FIND_TRAITOR_PROB || !event
        ? new StartEvent({ name: EventNames.MANAGER_PROTECTS })
        : new StartEvent({ name: EventNames.NO_CHANCE });
    })
  );

const stayHungry$ = (
  action$: ActionsObservable<FoodStolenActions>,
  state$: StateObservable<AppState>
): Observable<CommonActions> =>
  action$.pipe(
    ofType(FoodStolenActionTypes.STAY_HUNGRY),
    withLatestFrom(state$),
    map(([, state]) => {
      const cps = getCPS(state);
      return cps <= 1
        ? new AddIncome({ value: -300 })
        : new AddPassiveIncome({ value: -1 });
    })
  );

const foodStolenEventEnded$ = (
  action$: ActionsObservable<FoodStolenActions>
): Observable<EventActions | CommonActions | DemotedActions> =>
  action$.pipe(
    ofType(FoodStolenActionTypes.FOOD_STOLEN_ENDED),
    mapTo(new EndEvent())
  );

export const foodStolenEpic$ = combineEpics(
  onFoodStolenEvent$,
  foodStolenEventStarted$,
  foodStolenEvent$,
  findTraitor$,
  stayHungry$,
  foodStolenEventEnded$
);
