import { Action } from 'redux';

export enum FiredActionTypes {
  FIRED_STARTED = '[Fired event] Fired event started',
  FIRED_ENDED = '[Fired event] Fired event ended',
}

export class FiredStarted implements Action {
  readonly type = FiredActionTypes.FIRED_STARTED;
}

export class FiredEnded implements Action {
  readonly type = FiredActionTypes.FIRED_ENDED;
}

export type FiredActions = FiredStarted | FiredEnded;
