import { CommonState } from './reducer';
import { createSelector } from 'reselect';
import { AppState } from '../index';
import { Progress } from '../../components/progress-list/ProgressList';

const getCommonState = (state: AppState) => state.common;

export const getRoles = createSelector(
  getCommonState,
  (state: CommonState) => state.roles
);

export const getCurrentRole = createSelector(
  getCommonState,
  (state: CommonState) =>
    [...state.roles].filter(role => role.status === Progress.FINISHED).pop() ||
    null
);

export const getIncome = createSelector(
  getCommonState,
  (state: CommonState) => state.value
);

export const getCPS = createSelector(
  getCommonState,
  (state: CommonState) => state.cps
);

export const getGameStarted = createSelector(
  getCommonState,
  (state: CommonState) => state.started
);

export const getAutoPaddleEnabled = createSelector(
  getCommonState,
  (state: CommonState) => state.autoPaddleEnabled
);
