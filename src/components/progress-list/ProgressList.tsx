import React from 'react';
import './progress-list.scss';
import { zeroOrPositive } from '../../utils';

export enum Progress {
  IN_PROGRESS,
  FINISHED,
}

export interface Role {
  id: string;
  title: string;
  amount: number;
  status: Progress;
}

export interface ProgressListProps {
  items: Role[];
  current: Role;
  limit?: number;
}

export const getRoleStyle = (role: Role, currentRole: Role) =>
  !currentRole
    ? 'planned'
    : role.id === currentRole.id
    ? 'progress'
    : role.amount < currentRole.amount
    ? 'finished'
    : 'planned';
export const isDone = (roles: Role[], currentRole: Role) =>
  currentRole && roles[roles.length - 1].id === currentRole.id;
export const ProgressList = ({
  items,
  current,
  limit = 3,
}: ProgressListProps) => {
  if (!items || !items.length) {
    return null;
  }
  const index = !current ? 0 : items.findIndex(item => item.id === current.id);
  const list = items.slice(zeroOrPositive(index - 2), index + limit);
  return (
    <ul
      className={`progress-list-component ${isDone(items, current) && 'done'}`}
    >
      {list.map(role => (
        <li key={role.id} className="progress-item">
          <div
            className={`progress-list-item progress-list-item--${getRoleStyle(
              role,
              current
            )}`}
          >
            <span title={role.title} className="progress-role">
              {role.title}
            </span>
            <span>{role.amount}</span>
          </div>
        </li>
      ))}
    </ul>
  );
};

export default ProgressList;
