import { PaddleEvent, Priority } from '../../../types';
import { EndEvent, EventActions, EventNames, OpenEventBox } from '../actions';
import { ActionsObservable, combineEpics, ofType } from 'redux-observable';
import { Observable } from 'rxjs';
import { AddPassiveIncome, CommonActions } from '../../common';
import { map, switchMap } from 'rxjs/operators';
import { ofEventType } from '../../redux-helper';
import {
  SmokeBreakActions,
  SmokeBreakActionTypes,
  SmokeBreakEnded,
  SmokeBreakStarted,
} from './actions';
import { waitForAnswer } from '../operators';

export const smokeBreakEvent = new PaddleEvent(
  EventNames.SMOKE_BREAK,
  Priority.MEDIUM,
  {
    title: 'Перекур, работяги',
    denyText: 'Нет, лучше налягу с двойной силой',
    description: 'Коллеги предлагают сходить на перекур',
    submitText: 'Согласиться, не все же время грести',
  }
);

const smokeBreakStarted$ = (
  action$: ActionsObservable<EventActions>
): Observable<SmokeBreakActions> =>
  action$.pipe(
    ofEventType(EventNames.SMOKE_BREAK),
    map(() => {
      return new SmokeBreakStarted();
    })
  );

const smokeBreakEvent$ = (
  action$: ActionsObservable<SmokeBreakActions>
): Observable<EventActions | CommonActions> =>
  action$.pipe(
    ofType(SmokeBreakActionTypes.SMOKE_BREAK_STARTED),
    map(() => {
      return new OpenEventBox(smokeBreakEvent.details);
    })
  );

export const smokeBreakEnded$ = (
  action$: ActionsObservable<EventActions | SmokeBreakActions>
): Observable<SmokeBreakActions | CommonActions | EventActions> =>
  action$.pipe(
    ofType(SmokeBreakActionTypes.SMOKE_BREAK_STARTED),
    switchMap(() => waitForAnswer(action$)),
    // @todo Handle submit
    switchMap(result => [
      new AddPassiveIncome({ value: 1, time: 100000 }),
      new SmokeBreakEnded(),
      new EndEvent(),
    ])
  );

export const smokeBreakEpic$ = combineEpics(
  smokeBreakStarted$,
  smokeBreakEvent$,
  smokeBreakEnded$
);
