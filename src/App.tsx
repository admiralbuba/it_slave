import React, { Component } from 'react';
import './App.scss';
import Header from './components/header/Header';
import Footer from './components/footer/Footer';
import ProgressList, { Role } from './components/progress-list/ProgressList';
import Counter from './components/counter/Counter';
import Slave from './components/slave/Slave';
import PaddleActions from './components/paddle-actions/PaddleActions';
import EventBox from './components/event-box/EventBox';
import { connect } from 'react-redux';
import { mapDispatchToProps, mapStateToProps } from './redux';

export interface AppProps {
  incCounter: () => {};
  togglePassiveIncome: (active: boolean) => void;
  onAppInit: () => void;
  role: Role;
}

@(connect(
  mapStateToProps,
  mapDispatchToProps
) as any)
class App extends Component<
  AppProps &
    ReturnType<typeof mapStateToProps> &
    ReturnType<typeof mapDispatchToProps>
> {
  componentDidMount(): void {
    this.props.onAppInit();
  }

  render() {
    const {
      paddleClick,
      common,
      togglePassiveIncome,
      event,
      role,
      onDenyEvent,
      onSubmitEvent,
    } = this.props;
    return (
      <div className="app-wrapper">
        <Header role={role} />
        <main className="main-wrapper">
          <div className="event-box-container">
            <EventBox
              opened={event.opened}
              event={event.event}
              onDeny={onDenyEvent}
              onSubmit={onSubmitEvent}
            />
          </div>
          <div className="centered-container">
            <Counter value={common.value} cps={common.cps} />
            <Slave />
            <PaddleActions
              disabled={common.paddleDisabled}
              onClick={paddleClick}
              onAutoPaddleToggle={togglePassiveIncome}
              autoPaddleEnabled={common.autoPaddleEnabled}
            />
          </div>
          <div className="progress-list-container">
            <ProgressList items={common.roles} current={role} />
          </div>
        </main>
        <Footer />
      </div>
    );
  }
}

export default App;
