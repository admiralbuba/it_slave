import React from 'react';
import './counter.scss';

export interface CounterProps {
  value: number;
  cps: number;
}
export const Counter = ({ value, cps }: CounterProps) => (
  <div className="counter-component">
    <h1 className="counter-current-value">{value}</h1>
    <span>Пассивный инкам {cps}</span>
    <div className="counter-paddle">
      <span className="counter-paddle-text">ВЗМАХОВ</span>
    </div>
  </div>
);

export default Counter;
