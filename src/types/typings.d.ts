import {Action} from "src/redux";
import {Observable} from "rxjs";

declare module "redux-observable" {
    import { Observable } from "rxjs";
    type ActionByType<Union, Type> = Union extends { type: Type } ? Union : never;

    export function ofType<TActionTypes extends string[]>(
        ...key: TActionTypes
    ): <TAction extends Action<string>>(
        source: Observable<TAction>
    ) => Observable<ActionByType<TAction, TActionTypes[number]>>;
}
