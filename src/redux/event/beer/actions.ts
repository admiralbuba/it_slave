import { Action } from 'redux';

export enum BeerActionTypes {
  BEER_STARTED = '[Beer event] Beer event started',
  BEER_ENDED = '[Beer event] Beer event ended',
}

export class BeerStarted implements Action {
  readonly type = BeerActionTypes.BEER_STARTED;
}

export class BeerEnded implements Action {
  readonly type = BeerActionTypes.BEER_ENDED;
}

export type BeerActions = BeerStarted | BeerEnded;
