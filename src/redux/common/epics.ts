import { EMPTY, interval, merge, Observable, of, timer } from 'rxjs';
import {
  distinctUntilKeyChanged,
  filter,
  map,
  mapTo,
  mergeMap,
  switchMap,
  withLatestFrom,
} from 'rxjs/operators';
import {
  AddIncome,
  AddPassiveIncome,
  CalculateProgress,
  CommonActions,
  CommonActionTypes,
  GameStart,
  ResetIncome,
  SetProgress,
  TogglePassiveIncome,
} from './actions';
import {
  ActionsObservable,
  combineEpics,
  ofType,
  StateObservable,
} from 'redux-observable';
import { AppState } from '../index';
import { fromPayload } from '../redux-helper';
import { Progress } from '../../components/progress-list/ProgressList';
import {
  getAutoPaddleEnabled,
  getCPS,
  getGameStarted,
  getIncome,
  getRoles,
} from './selectors';

export const passiveIncome$ = (
  action$: ActionsObservable<CommonActions>,
  state$: StateObservable<AppState>
): Observable<CommonActions> =>
  action$.pipe(
    ofType(CommonActionTypes.TOGGLE_PASSIVE_INCOME),
    map(fromPayload),
    distinctUntilKeyChanged('active'),
    switchMap(({ active }) => (active ? interval(1000) : EMPTY)),
    withLatestFrom(state$),
    filter(([, state]) => getAutoPaddleEnabled(state)),
    map(([, state]) => getCPS(state)),
    map(value => new AddIncome({ value }))
  );

export const togglePassiveIncomeOnAutoPaddleChange = (
  action$: ActionsObservable<CommonActions>
): Observable<CommonActions> =>
  action$.pipe(
    ofType(CommonActionTypes.AUTO_PADDLE_TOGGLE),
    map(fromPayload),
    map(active => new TogglePassiveIncome(active))
  );

export const calculateProgressOnAddIncome$ = (
  action$: ActionsObservable<CommonActions>
): Observable<CommonActions> =>
  action$.pipe(
    ofType(CommonActionTypes.ADD_INCOME, CommonActionTypes.RESET_INCOME),
    mapTo(new CalculateProgress())
  );

export const calculateProgress$ = (
  action$: ActionsObservable<CommonActions>,
  state$: StateObservable<AppState>
): Observable<CommonActions> =>
  action$.pipe(
    ofType(CommonActionTypes.CALCULATE_PROGRESS),
    withLatestFrom(state$),
    map(([, state]) => {
      const [income, roles] = [getIncome(state), getRoles(state)];
      const currentProgress = roles.map(value => {
        if (value.amount <= income) {
          return { ...value, status: Progress.FINISHED };
        }
        return { ...value, status: Progress.IN_PROGRESS };
      });
      return new SetProgress(currentProgress);
    })
  );

export const appInit$ = (
  action$: ActionsObservable<CommonActions>
): Observable<CommonActions> =>
  action$.pipe(
    ofType(CommonActionTypes.APP_INIT),
    switchMap(() => of(new CalculateProgress()))
  );

export const startGame$ = (
  action$: ActionsObservable<CommonActions>,
  state$: StateObservable<AppState>
): Observable<CommonActions> =>
  action$.pipe(
    ofType(CommonActionTypes.ADD_INCOME),
    withLatestFrom(state$),
    filter(([, state]) => !getGameStarted(state)),
    mapTo(new GameStart())
  );

export const resetGame$ = (
  action$: ActionsObservable<CommonActions>
): Observable<CommonActions> =>
  action$.pipe(
    ofType(CommonActionTypes.GAME_END),
    mapTo(new ResetIncome())
  );

export const addPassiveIncome$ = (
  action$: ActionsObservable<CommonActions>
): Observable<CommonActions> =>
  action$.pipe(
    ofType(CommonActionTypes.ADD_PASSIVE_INCOME),
    filter(action => Boolean(action.payload.time)),
    map(fromPayload),
    mergeMap(({ time, value }) =>
      timer(time).pipe(map(() => new AddPassiveIncome({ value: -value })))
    )
  );

export const toggleTimerOnPaddleChange$ = (
  action$: ActionsObservable<CommonActions>
): Observable<CommonActions> =>
  merge(
    action$.pipe(
      ofType(CommonActionTypes.DISABLE_PADDLE),
      mapTo(false)
    ),
    action$.pipe(
      ofType(CommonActionTypes.ENABLE_PADDLE),
      mapTo(true)
    )
  ).pipe(map(active => new TogglePassiveIncome({ active })));

export const paddleClick$ = (
  action$: ActionsObservable<CommonActions>
): Observable<CommonActions> =>
  action$.pipe(
    ofType(CommonActionTypes.PADDLE_CLICK),
    mapTo(new AddIncome({ value: 1 }))
  );

export const commonEpic$ = combineEpics(
  passiveIncome$,
  paddleClick$,
  calculateProgress$,
  appInit$,
  calculateProgressOnAddIncome$,
  startGame$,
  resetGame$,
  addPassiveIncome$,
  toggleTimerOnPaddleChange$,
  togglePassiveIncomeOnAutoPaddleChange
);
