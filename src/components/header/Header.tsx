import React from "react";
import "./header.scss";
import { Role } from "../progress-list/ProgressList";

export interface HeaderProps {
  role: Role;
}

export const Header = ({ role }: HeaderProps) => (
  <header className="header-component">
    <div className="header-container">
      <h1 className="header-title header-title--logo wrapped-text">IT Slave</h1>
      <h1 className="header-title header-title--rank wrapped-text">
        {role && role.title}
      </h1>
    </div>
  </header>
);

export default Header;
