import { EventActionTypes, EventActions } from './actions';
import { EventDetails, PaddleEvent } from '../../types';

export interface EventState {
  opened: boolean;
  eventsCount: number;
  event: EventDetails;
  events: PaddleEvent[];
}

export const initialState: EventState = {
  opened: false,
  event: null,
  eventsCount: 0,
  events: [],
};

export default function eventBoxReducer(
  state: EventState = initialState,
  action: EventActions
) {
  switch (action.type) {
    case EventActionTypes.OPEN_EVENT_BOX:
      return { ...state, opened: true, event: action.payload };
    case EventActionTypes.CLOSE_EVENT_BOX:
      return { ...state, event: null, opened: false };
    case EventActionTypes.EVENT_START:
      return { ...state, eventsCount: state.eventsCount + 1 };
    case EventActionTypes.EVENT_END:
      return { ...state, eventsCount: state.eventsCount - 1 };
    case EventActionTypes.ADD_EVENTS:
      return { ...state, events: [...state.events, ...action.payload.events] };
  }
  return state;
}
