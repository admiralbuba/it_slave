import { Role } from '../../components/progress-list/ProgressList';

/**
 *
 * @param roles
 * @param currentRole
 * @param at number lover/above roles. -2 meas role lower than current in 1 position; +3 means upper that current in 3 positions
 */
export const getRoleAtDirection = (
  roles: Role[],
  currentRole: Role,
  at: number
) => {
  const sorted = roles.sort((a, b) => (a.amount - b.amount) * at);
  const len = sorted.length;
  const currentIndex = roles.findIndex(role => role.id === currentRole.id);
  const role = roles[currentIndex + Math.abs(at)];
  if (!role) {
    return at < 0 ? roles[len - 1] : roles[0];
  }
  return role;
};
