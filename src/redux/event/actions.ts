import { Action } from 'redux';
import { EventDetails, PaddleEvent, Priority } from '../../types';

export enum EventActionTypes {
  OPEN_EVENT_BOX = '[Event] Open event box',
  CLOSE_EVENT_BOX = '[Event] Close event box',
  ALL_EVENTS_ENDED = '[Event] All events ended',
  SUBMIT_EVENT = '[Event] Submit answer',
  DENY_EVENT = '[Event] Deny answer',
  TOGGLE_EVENT_TIMER = '[Event] Toggle event timer',
  EVENT_START = '[Event] Event start',
  EVENT_END = '[Event] Event end',
  CHANGE_PRIORITY = '[Event] Change priority',
  ADD_EVENTS = '[Event] Add events',
}

export enum EventNames {
  SMOKE_BREAK = 'Smoke break',
  FIRED = 'Fired',
  SUDDEN_MEETING = 'Sudden meeting',
  SUDDEN_MANAGER = 'Sudden manager',
  BEER = 'Beer',
  DEMOTED = 'Demoted',
  FOOD_STOLEN = 'Food stolen',
  FRIENDLY_MANAGER = 'Friendly manager',
  HOOKAH = 'Hookah',
  SECOND_PADDLE = 'Second paddle',
  NO_CHANCE = 'No chance',
  MANAGER_PROTECTS = 'Manager protects'
}

export class OpenEventBox implements Action {
  readonly type = EventActionTypes.OPEN_EVENT_BOX;
  constructor(public payload: EventDetails) {}
}

export class CloseEventBox implements Action {
  readonly type = EventActionTypes.CLOSE_EVENT_BOX;
}
export class SubmitEvent implements Action {
  readonly type = EventActionTypes.SUBMIT_EVENT;
}

export class DenyEvent implements Action {
  readonly type = EventActionTypes.DENY_EVENT;
}

export class StartEvent implements Action {
  readonly type = EventActionTypes.EVENT_START;
  constructor(public payload: { name: string }) {}
}

export class EndEvent implements Action {
  readonly type = EventActionTypes.EVENT_END;
}

export class ChangePriority implements Action {
  readonly type = EventActionTypes.CHANGE_PRIORITY;
  constructor(public payload: { name: string; priority: Priority }) {}
}

export class ToggleEventTimer implements Action {
  readonly type = EventActionTypes.TOGGLE_EVENT_TIMER;
  constructor(public payload: { active: boolean }) {}
}

export class AllEventsEnded implements Action {
  readonly type = EventActionTypes.ALL_EVENTS_ENDED;
}

export class AddEvents implements Action {
  readonly type = EventActionTypes.ADD_EVENTS;
  constructor(public payload: { events: PaddleEvent[] }) {}
}
export type EventActions =
  | OpenEventBox
  | CloseEventBox
  | SubmitEvent
  | DenyEvent
  | StartEvent
  | EndEvent
  | ChangePriority
  | ToggleEventTimer
  | AllEventsEnded
  | AddEvents;
