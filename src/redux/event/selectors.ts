import { AppState } from '../index';
import { createSelector } from 'reselect';
import { EventState } from './reducer';

const getEventState = (state: AppState) => state.event;

export const getActiveEventsCount = createSelector(
  getEventState,
  (state: EventState) => state.eventsCount
);

export const getEvents = createSelector(
  getEventState,
  (state: EventState) => state.events
);

export const getEvent = (state: AppState, name: string) =>
  getEvents(state).find(event => event.name === name);
