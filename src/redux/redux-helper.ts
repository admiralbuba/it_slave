import { Action } from 'redux';
import { ofType } from 'redux-observable';
import { filter, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { EventActionTypes } from './event/actions';

export const fromPayload = <T>(action: Action & { payload?: T }): T =>
  action.payload;

export function ofEventType(
  key: string
): <TAction extends Action<string>>(
  source: Observable<TAction>
) => Observable<any> {
  return source =>
    source.pipe(
      ofType(EventActionTypes.EVENT_START),
      map(fromPayload),
      filter(({ name }) => name === key)
    );
}