import { PaddleEvent } from '../../types';
import {
  AddEvents,
  AllEventsEnded,
  CloseEventBox,
  EventActions,
  EventActionTypes,
  StartEvent,
  ToggleEventTimer,
} from './actions';
import {
  ActionsObservable,
  combineEpics,
  ofType,
  StateObservable,
} from 'redux-observable';
import { EMPTY, interval, merge, Observable } from 'rxjs';
import {
  distinctUntilKeyChanged,
  filter,
  map,
  mapTo,
  switchMap,
  switchMapTo,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import { fromPayload } from '../redux-helper';
import {
  CommonActions,
  CommonActionTypes,
  DisablePaddle,
  EnablePaddle,
} from '../common';
import { firedEventEpic$ } from './fired/epic';
import { smokeBreakEpic$, smokeBreakEvent } from './smoke-break/epics';
import { suddenMeetingEpic$, suddenMeetingEvent } from './sudden-meeting/epics';
import { beerEpic$, beerEvent } from './beer/epics';
import { randomChoice } from '../../utils';
import { demotedEpic$ } from './demoted/epics';
import { AppState } from '../index';
import { getActiveEventsCount } from './selectors';
import { foodStolenEpic$, foodStolenEvent } from './food-stolen/epics';
import { getIncome } from '../common/selectors';

const events: PaddleEvent[] = [
  smokeBreakEvent,
  suddenMeetingEvent,
  beerEvent,
  foodStolenEvent,
];

const RANDOM_EVENT_TIMER = 10 * 1000;
const DISABLE_EVENT_AFTER_START_TIME = 30 * 1000;
const EVENT_PROBABILITY = 0.3;
const MIN_INCOME_TO_GET_EVENT = 10;

const toggleEventTimer$ = (
  action$: ActionsObservable<CommonActions & EventActions>
): Observable<EventActions> =>
  merge(
    action$.pipe(
      ofType(CommonActionTypes.GAME_START, EventActionTypes.ALL_EVENTS_ENDED),
      mapTo(true)
    ),
    action$.pipe(
      ofType(CommonActionTypes.GAME_END, EventActionTypes.EVENT_START),
      mapTo(false)
    )
  ).pipe(map(active => new ToggleEventTimer({ active })));

const allEventsEnded$ = (
  action$: ActionsObservable<EventActions>,
  state$: StateObservable<AppState>
): Observable<EventActions> =>
  action$.pipe(
    ofType(EventActionTypes.EVENT_END),
    withLatestFrom(state$),
    filter(([, state]) => getActiveEventsCount(state) <= 0),
    mapTo(new AllEventsEnded())
  );

const closeEventBoxOnEventEnd$ = (
  action$: ActionsObservable<EventActions>
): Observable<EventActions> =>
  action$.pipe(
    ofType(EventActionTypes.ALL_EVENTS_ENDED),
    mapTo(new CloseEventBox())
  );

const randomEventTimer$ = (
  action$: ActionsObservable<EventActions>,
  state$: StateObservable<AppState>
): Observable<EventActions> =>
  action$.pipe(
    ofType(EventActionTypes.TOGGLE_EVENT_TIMER),
    map(fromPayload),
    distinctUntilKeyChanged('active'),
    switchMap(active => (!active ? EMPTY : interval(RANDOM_EVENT_TIMER))),
    withLatestFrom(state$),
    filter(
      ([, state]) =>
        Math.random() <= EVENT_PROBABILITY ||
        getIncome(state) < MIN_INCOME_TO_GET_EVENT
    ),
    map(() => {
      const possibleEvents = events.filter(event => !event.isDisabled());
      return randomChoice(possibleEvents);
    }),
    map(({ name }) => new StartEvent({ name }))
  );

const disableEventAfterStart$ = (
  action$: ActionsObservable<EventActions>
): Observable<EventActions> =>
  action$.pipe(
    ofType(EventActionTypes.EVENT_START),
    map(fromPayload),
    tap(({ name }) => {
      const event = events.find(event => event.name === name);
      if (!event) {
        return;
      }
      event.disable(DISABLE_EVENT_AFTER_START_TIME);
    }),
    switchMapTo(EMPTY)
  );

const togglePaddleOnEvent$ = (
  action$: ActionsObservable<EventActions>
): Observable<CommonActions> =>
  merge(
    action$.pipe(
      ofType(EventActionTypes.EVENT_START),
      mapTo(new DisablePaddle())
    ),
    action$.pipe(
      ofType(EventActionTypes.ALL_EVENTS_ENDED),
      mapTo(new EnablePaddle())
    )
  );

export const onAppInit$ = (
  action$: ActionsObservable<CommonActions>
): Observable<EventActions> =>
  action$.pipe(
    ofType(CommonActionTypes.APP_INIT),
    map(() => new AddEvents({ events }))
  );

export const eventsEpic$ = combineEpics(
  toggleEventTimer$,
  randomEventTimer$,
  togglePaddleOnEvent$,
  disableEventAfterStart$,
  closeEventBoxOnEventEnd$,
  allEventsEnded$,
  onAppInit$,
  smokeBreakEpic$,
  firedEventEpic$,
  suddenMeetingEpic$,
  beerEpic$,
  demotedEpic$,
  foodStolenEpic$
);
