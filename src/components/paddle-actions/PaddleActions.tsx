import React from 'react';
import { Button } from 'antd';
import './paddle-actions.scss';

export interface ActionButtonProps {
  onClick: () => void;
  onAutoPaddleToggle: (active: boolean) => void;
  disabled?: boolean;
  autoPaddleEnabled?: boolean;
}
export const PaddleActions = ({
  onClick,
  onAutoPaddleToggle,
  disabled = false,
  autoPaddleEnabled = false,
}: ActionButtonProps) => (
  <div className="paddle-actions-group">
    <Button
      disabled={disabled}
      className="action-button action-btn"
      type="primary"
      shape="round"
      size="large"
      onClick={onClick}
    >
      ГРЕСТИ!
    </Button>
    <div className="paddle-auto">
      <span>Автоматическая гребля</span>
      <input
        disabled={disabled}
        type="checkbox"
        onChange={event => onAutoPaddleToggle(event.target.checked)}
      />
    </div>
  </div>
);

export default PaddleActions;
