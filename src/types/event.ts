export interface EventDetails {
  title: string;
  description: string;
  submitText: string;
  denyText: string;
}

export enum Priority {
  SPECIAL = 0,
  LOW = 1,
  BELOW_MEDIUM = 2,
  MEDIUM = 3,
  ABOVE_MEDIUM = 4,
  HIGH = 5,
  VERY_HIGH = 6,
}

export class PaddleEvent {
  private timerId: number;
  private disabled: boolean = false;
  private priority: Priority;
  constructor(
    public readonly name: string,
    priority: Priority = Priority.BELOW_MEDIUM,
    public readonly details: EventDetails,
    disabled = false
  ) {
    this.priority = priority;
    this.disabled = disabled;
  }

  disable(time?: number) {
    this.disabled = true;
    if (typeof time !== 'undefined') {
      this.timerId = (setTimeout(
        () => (this.disabled = false),
        time
      ) as unknown) as number;
    }
  }

  enable() {
    this.disabled = false;
    if (this.timerId) {
      clearTimeout(this.timerId);
    }
  }

  setPriority(priority: Priority) {
    this.priority = priority;
  }

  isDisabled(): boolean {
    return this.disabled;
  }
}
